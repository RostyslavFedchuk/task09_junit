package com.epam.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("MinesweeperTest")
public class MinesweeperTest {

    @DisplayName("Set Boolean Array Test")
    @ParameterizedTest
    @CsvFileSource(resources = "/values.csv", delimiter = ':')
    void testSetArray(int M, int N, double p) {
        Minesweeper minesweeper = new Minesweeper(M, N, p);
        Boolean[][] array = minesweeper.getArray();
        if(M <= 0 || N <= 0){
            assertNull(array);
        } else {
            assertNotNull(array);
            assertSame(M, array.length);
            assertSame(N, array[0].length);
        }
    }

    @DisplayName("Modified Array Test")
    @ParameterizedTest
    @CsvFileSource(resources = "/values.csv", delimiter = ':')
    void testCreateModifiedArray(int M, int N, double p){
        Minesweeper minesweeper = new Minesweeper(M, N, p);
        String[] modifiedArray = minesweeper.getModifiedArray();
        String[] modifiedArrayWithNumbers = minesweeper.getModifiedArrayWithNumbers();
        if(M <= 0 || N <= 0){
            assertNull(modifiedArray);
            assertNull(modifiedArrayWithNumbers);
        } else {
            getAssertion(M, N, modifiedArray);
            getAssertion(M, N, modifiedArrayWithNumbers);
        }
    }

    private void getAssertion(int M, int N, String[] modifiedArray) {
        assertNotNull(modifiedArray);
        assertEquals(M, modifiedArray.length);
        assertEquals(N * 2, modifiedArray[0].length());
    }
}
