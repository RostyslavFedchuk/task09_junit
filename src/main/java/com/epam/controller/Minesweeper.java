package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Stream;

import static java.util.Objects.nonNull;

public class Minesweeper {
    private static Logger logger = LogManager.getLogger(Minesweeper.class);
    private static Random random;
    private Boolean[][] array;
    private String[] modifiedArray;
    private String[] modifiedArrayWithNumbers;

    static {
        random = new Random();
    }

    public Minesweeper(int M, int N, double p){
        setArray(M,N,p);
    }

    public String[] getModifiedArray() {
        return modifiedArray;
    }

    public String[] getModifiedArrayWithNumbers() {
        return modifiedArrayWithNumbers;
    }

    private void setArray(int M, int N, double p) {
        if(M < 1 || N < 1 || p > 1 || p < 0){
            logger.info("Wrong input!!\n");
            return;
        }
        array = new Boolean[M][N];
        for (int i = 0; i < M; i++) {
            array[i] = Stream.generate(() -> random.nextDouble() < p)
                    .limit(N).toArray(Boolean[]::new);
        }
        createModifiedArray();
    }

    public Boolean[][] getArray(){
        return array;
    }

    private void createModifiedArray() {
        modifiedArray = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            modifiedArray[i] = "";
            for (int j = 0; j < array[i].length; j++) {
                modifiedArray[i] += array[i][j] ? "* " : ". ";
            }
        }
        createModifiedArrayWithNumbers();
    }

    private void createModifiedArrayWithNumbers() {
        modifiedArrayWithNumbers = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            modifiedArrayWithNumbers[i] = "";
            for (int j = 0; j < array[i].length; j++) {
                modifiedArrayWithNumbers[i] += array[i][j] ? "* " : countOfBombsNearby(i, j) + " ";
            }
        }
    }

    private int countOfBombsNearby(int x, int y) {
        return getCount(x - 1, y, x > 0) + getCount(x, y - 1, y > 0)
                + getCount(x + 1, y, x + 1 < array.length)
                + getCount(x, y + 1, y + 1 < array[0].length);
    }

    private int getCount(int x, int y, boolean condition) {
        return condition && array[x][y] ? 1 : 0;
    }

    public void printArrays() {
        if (nonNull(modifiedArray) && nonNull(modifiedArrayWithNumbers)) {
            logger.info("Your boolean array modified:\n");
            Arrays.stream(modifiedArray).forEach(s -> logger.info(s + "\n"));
            logger.info("Your boolean array modified with numbers:\n");
            Arrays.stream(modifiedArrayWithNumbers).forEach(s -> logger.info(s + "\n"));
        } else {
            logger.info("Your boolean array is empty!\n");
        }
    }
}
