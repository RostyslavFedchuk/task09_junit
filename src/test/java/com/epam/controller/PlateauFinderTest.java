package com.epam.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;

import java.lang.reflect.Field;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@DisplayName("PlateauFinderTest")
public class PlateauFinderTest {
    private static Logger logger = LogManager.getLogger(PlateauFinderTest.class);

    @DisplayName("Set integer array test")
    @Test
    public void testSetArray() {
        Integer[] array = new Integer[]{1, 1, 1, 1, 0};
        PlateauFinder sample = new PlateauFinder(array);
        assertArrayEquals(array, sample.getArray());
    }

    @Test
    @RepeatedTest(100)
    @DisplayName("Set random integer array test")
    public void testSetRandomArray() {
        int size = 10;
        PlateauFinder sample = new PlateauFinder(size);
        assertEquals(size, sample.getArray().length);
    }

    @Test
    @DisplayName("Test static final variables")
    public void testValues() {
        ResourceBundle bundle = ResourceBundle.getBundle("config");
        int minValue = Integer.valueOf(bundle.getString("MIN_VALUE"));
        int maxValue = Integer.valueOf(bundle.getString("MAX_VALUE"));
        int size = 10;
        PlateauFinder sample = new PlateauFinder(size);
        Class sampleClass = sample.getClass();
        testField(minValue, sample, sampleClass, "MIN_VALUE");
        testField(maxValue, sample, sampleClass, "MAX_VALUE");
    }

    private void testField(int minValue, PlateauFinder sample, Class sampleClass, String min_value) {
        try {
            Field minVal = sampleClass.getDeclaredField(min_value);
            minVal.setAccessible(true);
            assertEquals(minValue, minVal.getInt(sample));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            logger.info("NoSuchFieldException or IllegalAccessException");
        }
    }

    @Test
    public void testFindLongestPlateau() {
        PlateauFinder sample = mock(PlateauFinder.class);
        Integer[] array = new Integer[]{0, 1, 1, 0};
        PlateauFinder plateauFinder = new PlateauFinder(array);
        sample.setArray(array);
        doReturn("The longest plateau has length 2 and it is on interval: [1;2]\n")
                .when(sample).findLongestPlateau();
        assertEquals(plateauFinder.findLongestPlateau(), sample.findLongestPlateau());
        verify(sample).findLongestPlateau();
    }
}
